{
    "config": {
        "siteTitle": "Manupal.dev",
        "theme": "default",
        "defaultPage": "home-3",
        "login": "superadmin",
        "forceLogout": false,
        "password": "$2y$10$4IBwihdrP\/sQLAEwWC2YaeWXc.ED7l7oJkPjtsKY7\/95p6fMyWfNS",
        "lastLogins": {
            "2021\/09\/25 08:04:02": "103.195.202.102",
            "2021\/09\/25 07:49:01": "103.195.202.102",
            "2021\/09\/25 07:30:28": "103.195.202.102",
            "2021\/09\/25 07:28:23": "103.195.202.102",
            "2021\/09\/25 06:33:06": "103.195.202.102"
        },
        "defaultRepos": {
            "themes": {
                "0": "https:\/\/github.com\/robiso\/essence",
                "1": "https:\/\/github.com\/robiso\/violet",
                "2": "https:\/\/github.com\/robiso\/universeGradient",
                "3": "https:\/\/github.com\/robiso\/default",
                "4": "https:\/\/github.com\/StephanStanisic\/theme-w3css-singlepage",
                "5": "https:\/\/github.com\/StephanStanisic\/watercolor",
                "6": "https:\/\/github.com\/robiso\/material",
                "7": "https:\/\/github.com\/robiso\/parallax",
                "8": "https:\/\/github.com\/robiso\/oldDefault",
                "9": "https:\/\/github.com\/robiso\/abstract",
                "10": "https:\/\/github.com\/robiso\/bike",
                "11": "https:\/\/github.com\/robiso\/black",
                "12": "https:\/\/github.com\/robiso\/clean",
                "13": "https:\/\/github.com\/robiso\/carbide",
                "14": "https:\/\/github.com\/robiso\/darkBlue",
                "15": "https:\/\/github.com\/robiso\/fallout",
                "16": "https:\/\/github.com\/robiso\/gold",
                "17": "https:\/\/github.com\/robiso\/green",
                "18": "https:\/\/github.com\/robiso\/lightBlue",
                "19": "https:\/\/github.com\/robiso\/paper-l",
                "20": "https:\/\/github.com\/robiso\/paper-p",
                "21": "https:\/\/github.com\/robiso\/pink",
                "22": "https:\/\/github.com\/robiso\/purple",
                "23": "https:\/\/github.com\/robiso\/red",
                "24": "https:\/\/github.com\/robiso\/supermario",
                "25": "https:\/\/github.com\/robiso\/turbosDefault",
                "26": "https:\/\/github.com\/robiso\/wcms-theme-adivvyo"
            },
            "plugins": {
                "0": "https:\/\/github.com\/robiso\/summernote-editor",
                "1": "https:\/\/github.com\/robiso\/simple-blog",
                "2": "https:\/\/github.com\/robiso\/simple-statistics",
                "3": "https:\/\/github.com\/robiso\/additional-contents",
                "4": "https:\/\/github.com\/robiso\/hits-counter",
                "5": "https:\/\/github.com\/robiso\/cache-thumbs",
                "6": "https:\/\/github.com\/robiso\/summernote-air-editor",
                "7": "https:\/\/github.com\/robiso\/contact-form",
                "8": "https:\/\/github.com\/robiso\/translation-german",
                "9": "https:\/\/github.com\/robiso\/translation-russian",
                "10": "https:\/\/github.com\/robiso\/translation-dutch",
                "11": "https:\/\/github.com\/robiso\/translation-polish",
                "12": "https:\/\/github.com\/robiso\/translation-slovenian",
                "13": "https:\/\/github.com\/robiso\/translation-french",
                "14": "https:\/\/github.com\/robiso\/translation-spanish",
                "15": "https:\/\/github.com\/robiso\/translation-greek",
                "16": "https:\/\/github.com\/robiso\/translation-portuguese",
                "17": "https:\/\/github.com\/robiso\/translation-swedish",
                "18": "https:\/\/github.com\/robiso\/translation-serbian",
                "19": "https:\/\/github.com\/robiso\/translation-danish",
                "20": "https:\/\/github.com\/robiso\/translation-korean",
                "21": "https:\/\/github.com\/robiso\/translation-arabic",
                "22": "https:\/\/github.com\/robiso\/translation-hausa",
                "23": "https:\/\/github.com\/robiso\/translation-hindi"
            },
            "lastSync": "2021\/09\/25"
        },
        "customRepos": {
            "themes": {},
            "plugins": {}
        },
        "menuItems": {
            "0": {
                "name": "home",
                "slug": "home-3",
                "visibility": "hide"
            },
            "1": {
                "name": "aboutme",
                "slug": "aboutme",
                "visibility": "show"
            },
            "2": {
                "name": "projects",
                "slug": "projects-3",
                "visibility": "show"
            },
            "3": {
                "name": "contact",
                "slug": "contact",
                "visibility": "show"
            }
        }
    },
    "pages": {
        "404": {
            "title": "404",
            "keywords": "404",
            "description": "404",
            "content": "<h1>Sorry, page not found. :(<\/h1>"
        },
        "example": null,
        "aboutme": {
            "title": "Aboutme",
            "keywords": "Keywords, are, good, for, search, engines",
            "description": "A short description is also good.",
            "content": "<h2>About me<\/h2>"
        },
        "projects-3": {
            "title": "projects",
            "keywords": "Keywords, are, good, for, search, engines",
            "description": "A short description is also good.",
            "content": "<h1 class=\"mb-3\">About me<\/h1>"
        },
        "home-3": {
            "title": "home",
            "keywords": "Manupal Choudhary | Full Stack Developer and Programming Assistant @ IBPS Mumbai.",
            "description": "A short description is also good.",
            "content": "<div class=\"grid\">\n<img src=\"me.png\" class=\"w-32\">\n\n<div class=\"text-2xl\">\nHi, I'm Manupal Choudhary\n<\/div>\n<\/div>"
        },
        "contact": {
            "title": "Contact",
            "keywords": "Keywords, are, good, for, search, engines",
            "description": "A short description is also good.",
            "content": "<h2>contact us...<\/h2>"
        }
    },
    "blocks": {
        "subside": {
            "content": "Welcome to Manupal Choudhary's Personal Website."
        },
        "footer": {
            "content": "<span>©2021 Manupal Choudhary<\/span>"
        },
        "home_header": {
            "content": "\n\t<h1>Hi! <span>I'm Watercolor.<\/span><\/h1>\n\t<span>This is a detailed watercolor theme by <a href=\"https:\/\/steph.tools\">Stephan Stanisic<\/a>.<\/span>\n\n\t<div class=\"header-socials\">\n\t\t<a href=\"https:\/\/nitter.net\/\"><svg viewBox=\"0 0 496 512\"><use xlink:href=\"#fab-twitter\"><\/use><\/svg><\/a>\n\t\t<a href=\"https:\/\/joinmastodon.org\"><svg viewBox=\"0 0 496 512\"><use xlink:href=\"#fab-mastodon\"><\/use><\/svg><\/a>\n\t\t<a href=\"http:\/\/delta.chat\"><svg viewBox=\"0 0 496 512\"><use xlink:href=\"#fas-comment\"><\/use><\/svg><\/a>\n\t<\/div>\n\t"
        },
        "themes-watercolor-assets-jquery-min-js_header": {
            "content": "<h1>Page <span>Title.<\/span><\/h1>\n<span>And a subtitle<\/span>"
        },
        "aboutme_header": {
            "content": "<h1>Page <span>Title.<\/span><\/h1>\n<span>And a subtitle<\/span>"
        },
        "404_header": {
            "content": "<h1>four - oh - four<\/h1>\n<span>Page not found<\/span>"
        }
    }
}