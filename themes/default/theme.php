<?php global $Wcms ?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		<title><?= $Wcms->get('config', 'siteTitle') ?> - <?= $Wcms->page('title') ?></title>
		<meta name="description" content="<?= $Wcms->page('description') ?>">
		<meta name="keywords" content="<?= $Wcms->page('keywords') ?>">

		<!--
		  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		  <link rel="stylesheet" href="<?= $Wcms->asset('css/style.css') ?>"> 
		-->
		<link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">
		<link href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.css" rel="stylesheet">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote-lite.js"></script>


		<script>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;700;800&display=swap');
            
            body {
                font-family: 'Poppins', sans-serif;
				background-color : "white";
            }
		</script>
		<?= $Wcms->css() ?>
	</head>

	<body>
		<div class="bg-red-300">
			<?= $Wcms->settings() ?>
			<?= $Wcms->alerts() ?>
		</div>

		<!-- <nav class="navbar navbar-expand-lg navbar-light navbar-default">
			<div class="container">
				<a class="navbar-brand" href="<?= $Wcms->url() ?>">
				    <img src="https://i.imgur.com/VZjwUS6.png" alt="" class="w-32">
					<script>console.log("<?= $Wcms->get('config', 'siteTitle') ?>");</script> 
				</a>

				<div class="navbar-header">
				<button type="button" class="navbar-toggler navbar-toggle" data-toggle="collapse" data-target="#menu-collapse">
					<span class="navbar-toggler-icon">
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</span>
				</button>
				</div>

				<div class="collapse navbar-collapse" id="menu-collapse">
					<ul class="nav navbar-nav navbar-right ml-auto">
						<?= $Wcms->menu() ?>
					</ul>
				</div>
			</div>
		</nav> -->

<!-- Navbar using tailwind.css -->
<div class="tailwind-navbar p-4 mt-10 bg-blue-50 border-t-4 border-blue-900 shadow-lg ">
	<div class="container mx-auto">
		<div class="flex justify-between">
				<div class="lo">
					<a href="<?= $Wcms->url() ?>" class="">
						<img src="https://i.imgur.com/VZjwUS6.png" alt="" class="h-12">
					</a>
				</div>

				<div class="right_items flex items-center gap-3 px-4 py-2">
					<?= $Wcms->menu() ?>				
				</div>
		</div>	
	</div>	
</div>


		<center>
			<div class="container">
			
			<div class="intro px-20 py-24" style="background-image:url('https://i.imgur.com/PwCT2zx.png');">

			<center>
				<div class="flex text-left pl-10">
					<div class="my_image px-10 ">
						<img src="https://i.imgur.com/m1qI3Kh.png" alt="" class="w-32">
					</div>					

					<div class="text-2xl">
						<div class="title text-4xl font-light mb-3">
							Hi, I’m <b class="font-semibold">Manupal</b>
						</div>
						<div class="subtitle">
							a full stack developer in mumbai.
						</div>
					</div>
				</div>
			</center>
			

			</div>
			
				<?= $Wcms->page('content') ?>
			
				<!-- <?= $Wcms->block('subside') ?> -->
			</div>
		</center>

		<footer class="container-fluid bg-blue-50 border-b-4 border-blue-600 p-4 ">
			<center>
				<div class="container">
					<div class="text-right padding20 text-black text-xl">
						<?= $Wcms->footer() ?>
					</div>
				</div>
			</center>			
		</footer>

		<script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha384-nvAa0+6Qg9clwYCGGPpDQLVpLNn0fRaROjHqs13t4Ggj3Ez50XnGQqc/r8MhnRDZ" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous" type="text/javascript"></script>
		<?= $Wcms->js() ?>

	</body>
</html>
